package com.devcamp.rest_api.StringLengthAPI;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class StringLenaController {
    @GetMapping("/length")
    public int stringleng() {
        int leng;
        String str = "huong dan hoc java";

        String[] strArray = str.split("\\s");
        leng = strArray.length;

        return leng;
    }
}
